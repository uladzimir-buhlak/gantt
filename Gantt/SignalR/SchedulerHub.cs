﻿using Gantt.Models;
using Gantt.ViewModels;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using System.Collections.Generic;

namespace Gantt.SignalR
{
    [HubName("schedulerHub")]
    public class SchedulerHub : Hub
    {
        public void Update(List<UpdateModel> data, string id)
        {   
            var context = GlobalHost.ConnectionManager.GetHubContext<SchedulerHub>();
            context.Clients.AllExcept(id).update(data);
        }
    }
}