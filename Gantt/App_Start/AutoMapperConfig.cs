﻿using AutoMapper;
using Gantt.Models;
using Gantt.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Gantt.App_Start
{
    public class AutoMapperConfig
    {
        public static void RegisterMappings()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<Task, TaskDetails>()
                    .ForMember(x => x.start_date, y => y.MapFrom(x => x.StartDate.ToString("u")))
                    .ForMember(x => x.id, y => y.MapFrom(x => x.Id))
                    .ForMember(x => x.text, y => y.MapFrom(x => x.Text))
                    .ForMember(x => x.duration,  y => y.MapFrom(x => x.Duration))
                    .ForMember(x => x.progress, y => y.MapFrom(x => x.Progress))
                    .ForMember(x => x.sort_order, y => y.MapFrom(x => x.SortOrder))
                    .ForMember(x => x.type, y => y.MapFrom(x => x.Type))
                    .ForMember(x => x.parent_id, y => y.MapFrom(x => x.ParentId))
                    .ReverseMap();
                cfg.CreateMap<Link, LinkDetails>()
                .ForMember(x => x.id, y => y.MapFrom(x => x.Id))
                .ForMember(x => x.type, y => y.MapFrom(x => x.Type))
                .ForMember(x => x.source, y => y.MapFrom(x => x.SourceTaskId))
                .ForMember(x => x.target, y => y.MapFrom(x => x.TargetTaskId))
                .ReverseMap();
            });
        }
    }
}