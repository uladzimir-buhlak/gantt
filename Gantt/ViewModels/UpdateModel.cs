﻿using Gantt.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Gantt.ViewModels
{
    public class UpdateModel
    {
        public GanttAction Action { get; set; }

        public TaskDetails Task { get; set; }

        public LinkDetails Link { get; set; }
    }
}