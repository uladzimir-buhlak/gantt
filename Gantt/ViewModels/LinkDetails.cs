﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Gantt.ViewModels
{
    public class LinkDetails
    {
        public int id { get; set; }
        public string type { get; set; }
        public int source { get; set; }
        public int target { get; set; }
    }
}