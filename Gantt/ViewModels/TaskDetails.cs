﻿
namespace Gantt.ViewModels
{
    public class TaskDetails
    {
        public int id { get; set; }
        public string text { get; set; }
        public string start_date { get; set; }
        public int duration { get; set; }
        public decimal progress { get; set; }
        public int sort_order { get; set; }
        public string type { get; set; }
        public int? parent_id { get; set; }
    }
}