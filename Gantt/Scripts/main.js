﻿(function () {

    // add month scale
    gantt.config.scale_unit = "week";
    gantt.config.step = 1;
    gantt.config.order_branch = true;
    gantt.config.order_branch_free = true;

    gantt.templates.date_scale = function (date) {
        var dateToStr = gantt.date.date_to_str("%d %M");
        var endDate = gantt.date.add(gantt.date.add(date, 1, "week"), -1, "day");
        return dateToStr(date) + " - " + dateToStr(endDate);
    };
    gantt.config.subscales = [
        { unit: "day", step: 1, date: "%D" }
    ];
    gantt.config.scale_height = 50;

    // configure milestone description
    gantt.templates.rightside_text = function (start, end, task) {
        if (task.type == gantt.config.types.milestone) {
            return task.text;
        }
        return "";
    };
    // add section to type selection: task, project or milestone
    gantt.config.lightbox.sections = [
		{ name: "description", height: 70, map_to: "text", type: "textarea", focus: true },
		{ name: "type", type: "typeselect", map_to: "type" },
		{ name: "time", height: 72, type: "duration", map_to: "auto" }
    ];

    gantt.attachEvent("onRowDragEnd", function (id, target, obj) {
        console.log(target);
    });

    gantt.config.xml_date = "%Y-%m-%d %H:%i:%s"; // format of dates in XML
    gantt.init("ganttContainer"); // initialize gantt
    gantt.load("/Home/Data", "json");

    var hub = $.connection.schedulerHub, dp;

    $.connection.hub.start().done(function () {
        dp = new dataProcessor("/Home/Save?connectionId=" + $.connection.hub.id);
        dp.init(gantt);
    });

    var ganttAction = {
        inserted: 0,
        updated: 1,
        deleted: 2,
    }

    hub.client.update = function (list) {
        dp.ignore(function () {
            for (var i = 0; i < list.length; i++)
            {
                if (list[i].Task) {
                    updateTask(list[i].Task, list[i].Action);
                }
                if (list[i].Link) {
                    updateLink(list[i].Link, list[i].Action);
                }
            }
        });
    };

    function updateTask(task, action)
    {
        switch (action) {
            case ganttAction.inserted:
            case ganttAction.updated:
                gantt.addTask(task, task.parent_id);
                break;
            case ganttAction.deleted:
                if (gantt.isTaskExists(task.id))
                    gantt.deleteTask(task.id);
                break;
        }
    }

    function updateLink(link, action) {
        switch (action) {
            case ganttAction.inserted:
            case ganttAction.updated:
                gantt.addLink(link);
                break;
            case ganttAction.deleted:
                gantt.deleteLink(link.id);
                break;
        }
    }

})();